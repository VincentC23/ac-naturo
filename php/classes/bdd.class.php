<?php

class BDD {
    private $bdd;

    public function __construct (){
        $this->bdd = new PDO (
            'mysql:host=localhost;
            dbname=covi9041_SiteNaturo;
            charset=utf8',
            'covi9041',
            'gRWGfk6wMtfU',
            [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            ]
            );
    }
    
    public function selectOne(string $sql, array $param = []){
        $requete = $this->bdd->prepare($sql);
        $requete->execute($param);
        return $requete->fetch();
    }

    public function selectAll(string $sql, array $param = []){
        $requete = $this->bdd->prepare($sql);
        $requete->execute($param);
        return $requete->fetchAll();
    }

    public function getLastId(){
        return $this->bdd->lastInsertId();
    }

    public function update(string $sql, array $param = []){
        $requete = $this->bdd->prepare($sql);
        $requete->execute($param);
    }
}